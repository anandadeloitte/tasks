<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home</title>
</head>
<body>

	<header>User Task Management</header>
	<br>
	<br>
	<table class="table">
		<thead>
			<tr>
				<th scope="col">Description</th>
				<th scope="col">Updated time</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${user.tasks}" var="task">
				<tr>
					<td><p>${task.describtion}</p></td>
					<td><p>${task.updatedTime}</p></td>
					<td><form action="/deleteTask?task=${task.id}&userId=${user.id}"  method="post"><input type="submit" value="Delete"/></form></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

	<form action="addTask" method="post">
		<input type="text" name="describtion"><br>
		<input type="hidden" name="userId" value="${user.id}"><br>
		<input type="submit" value="Add new Task">
	</form>

</body>
</html>