package com.todolist.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.todolist.entity.Tasks;

public interface TasksRepo extends JpaRepository<Tasks, Integer>{
//List<Tasks> findByUserId(String id);
	void deleteById(Long id);
}
