package com.todolist.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.todolist.entity.User;

public interface UserRepo extends JpaRepository<User, Integer> {
	
	User findByUserName(String userName);
	User findById(Long id);

}
