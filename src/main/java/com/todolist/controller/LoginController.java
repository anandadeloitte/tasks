package com.todolist.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.todolist.dao.UserRepo;
import com.todolist.entity.User;

@Controller
public class LoginController {

	@Autowired
	UserRepo userRepo;

	@RequestMapping("/login")
	public String login() {

		return "login.jsp";
	}

	@RequestMapping("/userLogin")
	public ModelAndView userLogin(@RequestParam String userName, @RequestParam String password) {
		ModelAndView mv = null;
		User user = userRepo.findByUserName(userName);
		
		if (user != null && user.getPassword().equals(password)) {
			System.out.println("User Log in : "+user.getUserName());
			mv = new ModelAndView("home.jsp");
			mv.addObject("user", user);

		} else {
			mv = new ModelAndView("login.jsp");
			mv.addObject("errormsg", "Invalid Username or Password.");
		}

		return mv;
	}

}
