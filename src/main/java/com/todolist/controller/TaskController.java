package com.todolist.controller;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.todolist.dao.TasksRepo;
import com.todolist.dao.UserRepo;
import com.todolist.entity.Tasks;

@Controller
public class TaskController {

	@Autowired
	TasksRepo taskRepo;
	
	@Autowired
	UserRepo userRepo;

	@RequestMapping("/addTask")
	public ModelAndView addTask(Tasks task) {
		ModelAndView mv = new ModelAndView("home.jsp");;
		task.setId((long) taskRepo.count()+1);
		task.setStatus(false);
		task.setUpdatedTime(new Date());
		taskRepo.save(task);
		mv.addObject("user", userRepo.findById(task.getUserId()));
		return mv;
	}
	
	@Transactional
	@RequestMapping("/deleteTask")
	public ModelAndView deleteTask(@RequestParam Long task, @RequestParam Long userId) {
		ModelAndView mv = new ModelAndView("home.jsp");;
		taskRepo.deleteById(task);
		mv.addObject("user", userRepo.findById(userId));
		return mv;
	}

}
